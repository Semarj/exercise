import json

import pytest
import mock

import exercise

example_one = """
{
  "mentions": [
    "chris"
  ]
}"""

example_two = """
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}"""

example_three = """
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
    }
  ]
}"""

example_four = """
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}
"""
new_one = """
{
    "links": [
        {
            "url": "http://goo.gl/eiEC0O",
            "title": "Google"
        },
        {
            "url": "http://www.google.com",
            "title": "Google"
        }
    ]
}
"""

nbcolympics = mock.Mock(status_code=200, content="<title>NBC Olympics | 2014 NBC Olympics in Sochi Russia</title>")
twitter = mock.Mock(status_code=200, content="<title>Twitter / jdorfman: nice @littlebigdetail from ...</title>")
google1 = mock.Mock(status_code=200, content="<title>Google</title>")
google2 = mock.Mock(status_code=200, content="<badtitle>Google</brokentitle>")

parameterized = [
    ("@chris you around?", example_one, []),
    ("Good morning! (megusta) (coffee)", example_two, []),
    ("Olympics are starting soon; http://www.nbcolympics.com", example_three, [nbcolympics]),
    ("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016", example_four, [twitter]),
    ("test((megusta))", '{"emoticons": ["megusta"]}', []),
    ("test()emoticon", "{}", []),
    ("(longerthanfifteenreally)", "{}", []),
    ("(alone)", '{"emoticons": ["alone"]}', []),
    ("@alone", '{"mentions": ["alone"]}', []),
    ("(http://www.hipchat.com)", "{}", []),
    ("http://goo.gl/eiEC0O http://www.google.com", new_one, [google1, google1]),
    ("http://www.google.com/broken_html", "{}", [google2]),
    ("What is this site? http://www.google.com/asdf404Please", '{}', [mock.Mock(status_code=404)]),
]


@pytest.mark.parametrize("message,expected,requests_patch", parameterized)
def test(message, expected, requests_patch, mocker):
    mocker.patch("requests.get", side_effect=requests_patch)
    result_str = exercise.parse(message)
    assert json.loads(result_str) == json.loads(expected)
