import json
import re

import bs4
import requests

EMOTICON_START = "("
EMOTICON_END = ")"
EMOTICON_MAX_LENGTH = 15

MENTION_MAX_LENGTH = 50

alnum = re.compile("^[a-zA-Z0-9]+")

def parse(message):
    """Parse a message string for special components

    Args:
        message (unicode or str): message to be parsed
    Returns:
        str
    """
    if not isinstance(message, basestring):
        raise ValueError("This parses only string type classes")

    result = {}
    mentions = get_mentions(message)
    if mentions:
        result["mentions"] = mentions

    emoticons = get_emoticons(message)
    if emoticons:
        result["emoticons"] = emoticons

    links = get_links(message)
    if links:
        result["links"] = links

    return json.dumps(result)


def get_mentions(message):
    """
    Assumptions I would have asked questions about but that I mimicked behavior
    from hipchat.com instead:
    Shorter than 50 characters
    Cannot contain non-letter characters (allows alphanumeric)
    """
    mentions = []
    while message:
        _, _, message = message.partition("@")
        mention = alnum.match(message)
        if mention and len(mention.group()) <= MENTION_MAX_LENGTH:
            mentions.append(mention.group())

    return mentions


def get_emoticons(message):
    """
    Traverse the string once pulling out anything matching these requirements:
        contained in parenthesis
        less than 15 characters
        is alphanumeric: passes str.isalnum()
    """
    emoticons = []
    current = []
    started = False
    for char in message:
        if char == EMOTICON_START:
            current = []
            started = True
        elif char == EMOTICON_END:
            if len(current) > 0:
                emoticons.append(u"".join(current))
            current = []
            started = False
        elif started:
            if char.isalnum():
                current.append(char)
            else:
                started = False
                current = []
            # reset if we are over the emoticon limit
            if len(current) > EMOTICON_MAX_LENGTH:
                current = []
                started = False

    return emoticons


def get_links(message):
    """
    Assumptions I would have asked clarification about:
        URLs must be HTTP (over tcp or tls)
        URLs must be bounded by whitepsace
        URLs must resolve and be a 200 response
        redirects are followed but original location is returned in the "url"
    """
    links = []
    urls = [item for item in message.split() if item.startswith("http")]
    for url in urls:
        title = get_title(url)
        if title:
            links.append({
                "url": url,
                "title": title,
            })
    return links


def get_title(url):
    """ Get the title off of the page pointed to by a url.
    Args:
        url (str): url to be fetched

    Returns:
        str
    """
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException:
        return ""

    if response.status_code != 200:
        return ""

    title = bs4.BeautifulSoup(response.content, "html.parser").title
    if not title:
        return ""

    return title.getText()
